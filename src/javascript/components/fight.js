import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

    const { health: firstFighterHealth} = firstFighter;
    const { health: secondFighterHealth} = secondFighter;

    let firstFighterSuperAttackAvailable = true;
    let secondFighterSuperAttackAvailable = true;

    let firstFighterBlockAvailable = false;
    let secondFighterBlockAvailable = false;

    const firstFighterLineHealth = document.getElementById('left-fighter-indicator');
    const secondFighterLineHealth = document.getElementById('right-fighter-indicator');

    return new Promise((resolve) => {

      if (firstFighterSuperAttackAvailable) {
        useSuperAttack(
          () => {
            secondFighter.health -= getSuperPower(firstFighter);
            getFighterHealth(secondFighter, secondFighterHealth, secondFighterLineHealth);
          },
          ...controls.PlayerOneCriticalHitCombination
        );
        firstFighterSuperAttackAvailable = false;
        setTimeout(() => firstFighterSuperAttackAvailable = true, 10000);
      }

      if (secondFighterSuperAttackAvailable) {
        useSuperAttack(
          () => {
            firstFighter.health -= getSuperPower(secondFighter);
            getFighterHealth(firstFighter, firstFighterHealth, firstFighterLineHealth);
          },
          ...controls.PlayerTwoCriticalHitCombination
        );
        secondFighterSuperAttackAvailable = false;
        setTimeout(() => secondFighterSuperAttackAvailable = true, 10000);
      }

      document.addEventListener('keydown', function(e) {
        switch (e.code) {
          case controls.PlayerOneBlock:
            firstFighterBlockAvailable = true;
            break;
          case controls.PlayerTwoBlock:
            secondFighterBlockAvailable = true;
            break;
        }
      });

      document.addEventListener('keyup', function(e) {

        switch (e.code) {
          case controls.PlayerOneBlock:
            firstFighterBlockAvailable = false;
            break;
          case controls.PlayerTwoBlock:
            secondFighterBlockAvailable = false;
            break;
          case controls.PlayerOneAttack:
            if (!firstFighterBlockAvailable) {

              if(!secondFighterBlockAvailable) {
                secondFighter.health -= getDamage(firstFighter, secondFighter);
              }

              getFighterHealth(secondFighter, secondFighterHealth, secondFighterLineHealth);
            }
            break;
          case controls.PlayerTwoAttack:
            if (!secondFighterBlockAvailable) {
              if(!firstFighterBlockAvailable) {
                firstFighter.health -=  getDamage(secondFighter, firstFighter)
              }
              getFighterHealth(firstFighter, firstFighterHealth, firstFighterLineHealth);
            }
            break;
        }

        if (firstFighter.health <= 0 ) {
          resolve(secondFighter)
        } else if (secondFighter.health <= 0) {
          resolve(firstFighter)
        }

      });
  });
}

export function getDamage(attacker, defender) {

  let damage = getHitPower(attacker) - getBlockPower(defender);

  damage = damage >= 0 ? damage : 0;
  return damage
}

export function getSuperPower(fighter) {
  let power = fighter.attack * 2;
  return power;
}

export function getHitPower(fighter) {
  const power = fighter.attack * (1 + Math.random());
  return power;
}

export function getBlockPower(fighter) {
  const block =  fighter.defense * (1 + Math.random());
  return block;
}

function getFighterHealth(fighter, initialFighterHealth, lineHealth) {
  const result  = fighter.health / initialFighterHealth * 100;

  if (result > 0) {
    lineHealth.style.width = `${result}%`;
  } else {
    lineHealth.style.width = `0%`;
  }
  return result;
}

function useSuperAttack(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();
    func();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });
}