import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.append(createFighterImage(fighter), fighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function oneFighterInfoFeature(key, value) {

  const feature = createElement({
    tagName: "div",
    className: "fighter-preview__feature"
  });

  const keyOfFeature = createElement({tagName: "span"});
  keyOfFeature.innerHTML = key;
  const valueOfFeature = createElement({
    tagName: "span",
    className: "fighter-preview__feature-value"
  });
  valueOfFeature.innerHTML = value;

  feature.append(keyOfFeature, valueOfFeature);

  return feature
}

function fighterInfo(fighter) {
  const container = createElement({
    tagName: "div",
    className: "fighter-preview__info-container"
  });

  const name = createElement({
    tagName: "span",
    className: "fighter-preview__info-name"
  });

  const health = oneFighterInfoFeature("Health: ", fighter.health);
  const attack = oneFighterInfoFeature("Attack: ", fighter.attack);
  const defense = oneFighterInfoFeature("Defense: ", fighter.defense);

  name.innerHTML = fighter.name.toUpperCase();
  container.append(name, health, attack, defense);

  return container;
}